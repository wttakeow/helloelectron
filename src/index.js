const electron = require('electron')
const path = require('path')
const ipc = electron.ipcRenderer
const BrowserWindow = electron.remote.BrowserWindow

const notifyBtn = document.getElementById('notifyBtn')

var chatMSG = document.getElementById('chatMSG')

var updateBtn = document.getElementById('updateBtn')

const modalPath = path.join('file://', __dirname, 'add.html')
let win = new BrowserWindow({ frame: false, width: 400, height: 200 })
win.on('close', function () { win = null })
win.loadURL(modalPath)

notifyBtn.addEventListener('click', function (event) {
    
    win.show()
})

updateBtn.addEventListener('click', function () {
    ipc.send('update-to-mini', document.getElementById('chatbox').value)
  })


ipc.on('revMSG', function (event, arg) {
    chatMSGval = String(arg);
    chatMSG.innerHTML = chatMSGval.toLocaleString('en');
})

ipc.on('forwardToChild', function (event, arg) {
    win.webContents.send('revMSG', arg)
    console.log("HELLO")
})


